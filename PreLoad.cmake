# <copyright company="CAScination AG">
#     This file contains proprietary code. Copyright (c) 2021. All rights reserved
# </copyright>

# Makes -G "Visual Studio 15 2017 Win64" default, no need to specify generator and target at first call
set(CMAKE_GENERATOR_TOOLSET "host=x64" CACHE INTERNAL "" FORCE)
set(CMAKE_GENERATOR "Visual Studio 16 2019" CACHE INTERNAL "" FORCE)
