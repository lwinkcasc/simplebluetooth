#include <QCoreApplication>
#include <QtBluetooth/qbluetoothaddress.h>
#include <QtBluetooth/qbluetoothlocaldevice.h>
#include <QBluetoothServiceDiscoveryAgent>
#include <QBluetoothDeviceInfo>
#include <QDebug>
#include <QObject>

#include <iostream>
#include <chrono>
#include <thread>

int main(int argc, char* argv[])
{
    QCoreApplication a(argc, argv);

	QBluetoothLocalDevice localDevice;
     
    if (!localDevice.isValid()) {
        std::cout << "Bluetooth local device not found" << std::endl;
    }

    localDevice.powerOn();
    localDevice.setHostMode(QBluetoothLocalDevice::HostDiscoverable);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    std::string deviceName = localDevice.name().toStdString();
    std::string deviceAddress = localDevice.address().toString().toStdString();

    std::cout << "Found bluetooth local device: " << deviceName << " with address " << deviceAddress << std::endl;
	
	/*------------------------------------------*/

    QBluetoothDeviceDiscoveryAgent* discoveryAgent = new QBluetoothDeviceDiscoveryAgent();

    // Connect the signal to a lambda function
    // The 3rd param is a dummy one, in real life application it will be an instance that point to the slot (4th param) owner
    QObject::connect(discoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, new QObject(),
                     [](const QBluetoothDeviceInfo& device) {
                         qInfo() << QString("Device found!! Its name is %1 and its MAC is %2").arg(device.name(), device.address().toString());
                     });

    // Stop after 5000 mS
    //discoveryAgent->setLowEnergyDiscoveryTimeout(5000);
    // Start the discovery process
    discoveryAgent->start(/* QBluetoothDeviceDiscoveryAgent::LowEnergyMethod*/);

    int retCode = a.exec();

    delete discoveryAgent;

    return retCode;
}